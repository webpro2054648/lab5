type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: string //male,female,others
    roles: Role[] //admin,user
}

export type { Gender, Role, User }